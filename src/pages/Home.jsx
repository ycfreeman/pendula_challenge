import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import {
  TheNavigator,
  TheNavigatorSection,
  useTheNavigatorSection
} from "@/components/TheNavigator";
import UserForm from "./VaildaForm";

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const SectionContent = ({ title }) => {
  const { active, error, reset, next } = useTheNavigatorSection();

  return (
    <UserForm
      title={title}
      disabled={!active}
      onSubmit={({ valid }) => {
        if (!valid) {
          error();
        } else {
          next();
        }
      }}
      onError={valid => {
        if (active) {
          if (!valid) {
            error();
          } else {
            reset();
          }
        }
      }}
    />
  );
};

export default function Page() {
  const classes = useStyles();

  return (
    <Container component="main">
      <CssBaseline />
      <div className={classes.paper}>
        <div className={classes.form}>
          <TheNavigator>
            <TheNavigatorSection description="Section 1">
              <SectionContent title="Section 1" />
            </TheNavigatorSection>
            <TheNavigatorSection description="Section 2">
              <SectionContent title="Section 2" />
            </TheNavigatorSection>
            <TheNavigatorSection description="Section 3">
              <SectionContent title="Section 3" />
            </TheNavigatorSection>
          </TheNavigator>
        </div>
      </div>
    </Container>
  );
}
