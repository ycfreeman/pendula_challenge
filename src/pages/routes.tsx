import HomeIcon from "@material-ui/icons/Home";

import Home from "./Home";
import Tabs from "./Tabs";

const routes = [
  {
    path: "/",
    exact: true,
    component: Home,
    title: "1",
    Icon: HomeIcon
  },
  {
    path: "/tabs",
    component: Tabs,
    title: "2",
    Icon: HomeIcon
  }
];

export default routes;
