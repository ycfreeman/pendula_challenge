// code mainly from https://github.com/highercomve/react-valida-hook modified with material ui form fields

import React from "react";
import useValitedForm from "react-valida-hook";

import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from "@material-ui/core/TextField";
import FormLabel from "@material-ui/core/FormLabel";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const initialState = {
  firstName: "",
  lastName: "",
  email: ""
};

const validations = [
  {
    name: "firstName",
    type: "required",
    stateMap: "firstName"
  },
  {
    name: "lastName",
    type: "required",
    stateMap: "lastName"
  },
  {
    name: "email",
    type: "required",
    stateMap: "email"
  },
  {
    name: "email",
    type: "isEmail",
    stateMap: "email"
  }
];

function UserForm({
  title = "",
  disabled = false,
  onSubmit = () => {},
  onError = () => {}
}) {
  const [formData, validation, validateForm, getData] = useValitedForm(
    initialState,
    validations
  );

  const formIsValid = React.useMemo(() => validation.valid, [validation.valid]);

  React.useEffect(() => {
    onError(formIsValid);
    // we can trigger this callback by different effects here
    // say we don't want a submit button, we can have some logic here to check for completion + error checking for each section
  }, [onError, formIsValid]);

  const submit = event => {
    event.preventDefault();
    const valid = validateForm();
    console.log(getData());
    onSubmit({ valid, formData: getData() });
  };
  return (
    <form noValidate={true} onSubmit={submit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel>{title}</FormLabel>
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            disabled={disabled}
            autoComplete="fname"
            name="firstName"
            variant="outlined"
            required
            fullWidth
            id="firstName"
            label="First Name"
            autoFocus
            error={!!validation.errors.firstName.join(", ")}
            {...formData.firstName.input}
          />
          <FormHelperText error={!!validation.errors.firstName.join(", ")}>
            {validation.errors.firstName.join(", ")}
          </FormHelperText>
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            disabled={disabled}
            variant="outlined"
            required
            fullWidth
            id="lastName"
            label="Last Name"
            name="lastName"
            autoComplete="lname"
            error={!!validation.errors.lastName.join(", ")}
            {...formData.lastName.input}
          />
          <FormHelperText error={!!validation.errors.lastName.join(", ")}>
            {validation.errors.lastName.join(", ")}
          </FormHelperText>
        </Grid>
        <Grid item xs={12}>
          <TextField
            disabled={disabled}
            variant="outlined"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            error={!!validation.errors.email.join(", ")}
            {...formData.email.input}
          />
          <FormHelperText error={!!validation.errors.email.join(", ")}>
            {validation.errors.email.join(", ")}
          </FormHelperText>
        </Grid>
      </Grid>
      <div>
        <Button
          disabled={disabled}
          variant="contained"
          type="submit"
          value="Submit"
          color="primary"
        >
          Next
        </Button>
      </div>
    </form>
  );
}

export default UserForm;
