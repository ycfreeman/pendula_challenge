import React from "react";
import clsx from "clsx";

import { TheNavigatorStepStatus } from "./constants";

const iconClasses = {
  [TheNavigatorStepStatus.Available]: "fas fa-dot-circle",
  [TheNavigatorStepStatus.Completed]: "fas fa-check-circle",
  [TheNavigatorStepStatus.Invalid]: "fas fa-exclamation-circle",
  [TheNavigatorStepStatus.Unavailable]: "far fa-dot-circle"
};

export const TheNavigatorIcon: React.FC<{
  status: TheNavigatorStepStatus;
  className?: string;
}> = ({ status, className }) => {
  return <i className={clsx([iconClasses[status], className])} />;
};
