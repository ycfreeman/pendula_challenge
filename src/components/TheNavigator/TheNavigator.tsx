import React from "react";

import "@fortawesome/fontawesome-free/css/all.css";

import { makeStyles } from "@material-ui/core/styles";

import {
  TheNavigatorStepStatus,
  TheNavigatorStep,
  theNavigatorContext
} from "./constants";
import { reducer, setActiveAC, setStatusAC } from "./widgets";

import { TheNavigatorSection } from "./TheNavigatorSection";

const useStyles = makeStyles(theme => ({
  container: {
    display: "grid",
    gridTemplateColumns: "min-content auto",
    gridAutoFlow: "column"
  }
}));

// components
export const TheNavigator: React.FC = ({ children }) => {
  if (
    !React.Children.map(children, c => {
      return React.isValidElement(c) && c.type === TheNavigatorSection;
    }).reduce((prev, curr) => prev && curr, true)
  ) {
    throw Error("only <TheNavigatorSection />s are allowed");
  }

  const initState = React.Children.map(
    children,
    (c, i): TheNavigatorStep => {
      if (i === 0) {
        return {
          status: TheNavigatorStepStatus.Available,
          active: true
        };
      }
      return {
        status: TheNavigatorStepStatus.Unavailable,
        active: false
      };
    }
  );

  const [state, dispatch] = React.useReducer(reducer, initState);
  const maxSteps = React.Children.count(children);

  function activate(i: number) {
    dispatch(setActiveAC(i));
    if (state[i].status === TheNavigatorStepStatus.Unavailable) {
      dispatch(setStatusAC(i, TheNavigatorStepStatus.Available));
    }
  }

  function reset(i: number) {
    dispatch(setStatusAC(i, TheNavigatorStepStatus.Available));
  }

  function error(i: number) {
    dispatch(setStatusAC(i, TheNavigatorStepStatus.Invalid));
  }

  function hasNext(i: number) {
    return i < maxSteps - 1;
  }

  function next(i: number) {
    dispatch(setStatusAC(i, TheNavigatorStepStatus.Completed));
    if (hasNext(i)) {
      activate(i + 1);
    } else {
      dispatch(setActiveAC(i, false)); // deactivate current step
    }
  }

  const classes = useStyles();

  return (
    <div className={classes.container}>
      {React.Children.map(children as React.ReactElement[], (c, i) => {
        return (
          <theNavigatorContext.Provider
            value={{
              status: state[i].status,
              nextStatus: hasNext(i) ? state[i + 1].status : undefined,
              active: state[i].active,
              activate() {
                activate(i);
              },
              reset() {
                reset(i);
              },
              error() {
                error(i);
              },
              next() {
                next(i);
              }
            }}
          >
            {React.cloneElement(c)}
          </theNavigatorContext.Provider>
        );
      })}
    </div>
  );
};
