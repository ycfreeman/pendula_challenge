import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { theNavigatorContext, TheNavigatorStepStatus } from "./constants";
import { TheNavigatorIcon } from "./TheNavigatorIcon";
import clsx from "clsx";
import { CSSTransition } from "react-transition-group";

const useStyles = makeStyles(theme => ({
  sectionNavContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    paddingRight: 30,
    gridColumn: 1
  },
  sectionIconContainer: {
    position: "relative"
  },
  sectionIcon: {
    fontSize: 30,
    paddingTop: 3,
    paddingBottom: 3,
    cursor: "pointer",
    transition: "color ease-in-out .3s, background-color ease-in-out .3s"
  },
  sectionLine: {
    flexGrow: 1,
    width: 3,
    minHeight: 20,
    transition: "color ease-in-out .3s, background-color ease-in-out .3s"
  },
  activeText: {
    color: "#6dc5ab"
  },
  activeBg: {
    background: "#6dc5ab",
    color: "#ffffff",
    "&:after": {
      borderRightColor: "#6dc5ab"
    }
  },
  availableText: {
    color: "#d2ddef"
  },
  availableBg: {
    background: "#d2ddef",
    "&:after": {
      borderRightColor: "#d2ddef"
    }
  },
  unavailableText: {
    color: "#d2ddef"
  },
  unavailableBg: {
    background: "#d2ddef",
    "&:after": {
      borderRightColor: "#d2ddef"
    }
  },
  completedText: {
    color: "#384b5d"
  },
  completedBg: {
    background: "#384b5d",
    color: "#ffffff",
    "&:after": {
      borderRightColor: "#384b5d"
    }
  },
  invalidText: {
    color: "#c05f68"
  },
  invalidBg: {
    background: "#c05f68",
    color: "#ffffff",
    "&:after": {
      borderRightColor: "#c05f68"
    }
  },
  tooltipContainer: {
    position: "absolute",
    display: "flex",
    left: "100%",
    marginLeft: 12,
    alignItems: "center",
    top: 0,
    bottom: 0
  },
  tooltip: {
    display: "inline-block",
    whiteSpace: "nowrap",
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 3,
    "&:after": {
      display: "inline-block",
      right: "100%",
      top: "50%",
      content: "' '",
      height: 0,
      width: 0,
      position: "absolute",
      pointerEvents: "none",
      borderWidth: 8,
      marginTop: -8,
      borderStyle: "solid",
      borderLeftColor: "transparent",
      borderTopColor: "transparent",
      borderBottomColor: "transparent"
    }
  },
  tooltipEnter: {
    opacity: 0,
    transform: "scale(0.9)"
  },
  tooltipEnterActive: {
    opacity: 1,
    transform: "translateX(0)",
    transition: "opacity 200ms, transform 200ms"
  },
  tooltipExit: {
    opacity: 1
  },
  tooltipExitActive: {
    opacity: 0,
    transform: "scale(0.9)",
    transition: "opacity 200ms, transform 200ms"
  }
}));

interface TheNavigatorSectionProps {
  description?: React.ReactNode;
}

export const useTheNavigatorSection = () => {
  const { nextStatus, ...props } = React.useContext(theNavigatorContext);
  return props;
};

interface TheNavigatorSectionTooltipProps {
  show: boolean;
  className?: string;
}

const TheNavigatorSectionTooltip: React.FC<TheNavigatorSectionTooltipProps> = ({
  show = false,
  className,
  children
}) => {
  const classes = useStyles();
  return (
    <CSSTransition
      in={show}
      timeout={200}
      classNames={{
        enter: classes.tooltipEnter,
        enterActive: classes.tooltipEnterActive,
        exit: classes.tooltipExit,
        exitActive: classes.tooltipExitActive
      }}
      mountOnEnter
      unmountOnExit
    >
      <div className={classes.tooltipContainer}>
        <div className={clsx([classes.tooltip, className])}>{children}</div>
      </div>
    </CSSTransition>
  );
};

export const TheNavigatorSection: React.FC<TheNavigatorSectionProps> = ({
  children,
  description
}) => {
  const { nextStatus, ...props } = React.useContext(theNavigatorContext);
  const { activate, status, active } = props;

  const [showTooltip, setShowTooltip] = React.useState(false);

  const classes = useStyles();

  if (!!children) {
    return (
      <>
        <div className={classes.sectionNavContainer}>
          <span
            className={classes.sectionIconContainer}
            onClick={
              status !== TheNavigatorStepStatus.Unavailable
                ? () => activate()
                : undefined
            }
            onMouseEnter={() => setShowTooltip(true)}
            onMouseOut={() => setShowTooltip(false)}
          >
            <TheNavigatorIcon
              status={status}
              className={clsx([
                classes.sectionIcon,
                active ? classes.activeText : (classes as any)[`${status}Text`]
              ])}
            />
            <TheNavigatorSectionTooltip
              show={showTooltip}
              className={
                active ? classes.activeBg : (classes as any)[`${status}Bg`]
              }
            >
              {description}
            </TheNavigatorSectionTooltip>
          </span>
          {nextStatus ? (
            <span
              className={clsx([
                classes.sectionLine,
                (classes as any)[`${nextStatus}Bg`]
              ])}
            />
          ) : null}
        </div>
        {children}
      </>
    );
  }
  return null;
};
