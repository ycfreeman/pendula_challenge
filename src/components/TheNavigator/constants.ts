import React from "react";

export enum TheNavigatorStepStatus {
  Available = "available",
  Unavailable = "unavailable",
  Completed = "completed",
  Invalid = "invalid"
}

export interface TheNavigatorStep {
  status: TheNavigatorStepStatus;
  active: boolean;
}

export interface TheNavigatorSectionContext extends TheNavigatorStep {
  nextStatus?: TheNavigatorStepStatus;
  activate(): void; // set section to active
  reset(): void; // set section to available (after error)
  error(): void; // set section to error
  next(): void; // set current section to complete and activate next section
}

export const theNavigatorContext = React.createContext<
  TheNavigatorSectionContext
>({
  status: TheNavigatorStepStatus.Unavailable,
  active: false,
  activate() {},
  reset() {},
  error() {},
  next() {}
});
