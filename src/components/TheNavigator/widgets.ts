import { TheNavigatorStep, TheNavigatorStepStatus } from "./constants";

// actions, reducers

interface Action<P> {
  type: string;
  payload: P;
}

// action creators
export const setStatusAC = (index: number, status: TheNavigatorStepStatus) => ({
  type: "SET_STATUS",
  payload: { index, status }
});

export const setActiveAC = (index: number, active: boolean = true) => ({
  type: "SET_ACTIVE",
  payload: { index, active }
});

// reducers

const reduceSetStatus = (
  state: TheNavigatorStep[],
  action: Action<{ index: number; status: TheNavigatorStepStatus }>
) => {
  const { index, status } = action.payload;
  const newState = [...state];
  newState[index].status = status;
  return newState;
};

const reduceSetActive = (
  state: TheNavigatorStep[],
  action: Action<{
    index: number;
    active: boolean; // default true
  }>
) => {
  const { index, active } = action.payload as {
    index: number;
    active: boolean; // default true
  };
  let newState = [...state];
  newState = newState.map(s => ({ ...s, active: false }));
  newState[index].active = active;
  return newState;
};

export const reducer = (state: TheNavigatorStep[], action: Action<any>) => {
  switch (action.type) {
    case "SET_STATUS": {
      return reduceSetStatus(state, action);
    }
    case "SET_ACTIVE": {
      return reduceSetActive(state, action);
    }
    default:
      return state;
  }
};
