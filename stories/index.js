import React from "react";
import { storiesOf } from "@storybook/react";

import {
  TheNavigator,
  TheNavigatorSection,
  useTheNavigatorSection
} from "../src/components/TheNavigator";

const SectionContent = () => {
  const { active, status, reset, error, next } = useTheNavigatorSection();
  return (
    <div>
      {`${active}, ${status}`}
      <button onClick={() => error()}>error</button>
      <button onClick={() => reset()}>reset</button>
      <button onClick={() => next()}>next</button>
    </div>
  );
};

storiesOf("TheNavigator", module).add("basic", () => (
  <TheNavigator>
    <TheNavigatorSection description="section 1">
      <SectionContent />
    </TheNavigatorSection>
    <TheNavigatorSection description="section 1">
      <SectionContent />
    </TheNavigatorSection>
    <TheNavigatorSection description="section 1">
      <SectionContent />
    </TheNavigatorSection>
    <TheNavigatorSection description="section 1">
      <SectionContent />
    </TheNavigatorSection>
    <TheNavigatorSection
      description={
        <div style={{ color: "red" }}>
          <pre>{`this
tooltip is a custom element and
has multiple
lines`}</pre>
        </div>
      }
    >
      <SectionContent />
    </TheNavigatorSection>
  </TheNavigator>
));
