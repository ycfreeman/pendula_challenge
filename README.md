### getting started

```
yarn
yarn start
```

it should start storybook and example project together

### note
typescript is used to write the component, that should document pretty well how it works  
example inside `stories/index.js` should show all available apis  
`src/` contains example of how it can be used in an actual app

since the exercise is pretty cosmetic, didn't spend time on writing tests   
however tests can be written to test the state manipulations

performance was not considered. having multiple context may have performance if there are many sections   
and didn't spend too much time to make sure there's no unintended re-renders
